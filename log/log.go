// from the blog https://www.goinggo.net/2013/11/using-log-package-in-go.html
// https://www.ardanlabs.com/blog/2013/11/using-log-package-in-go.html
package log

import (
	"io"
	"io/ioutil"
	"log"
)

var (
	Trace   *log.Logger
	Info    *log.Logger
	Warning *log.Logger
	Error   *log.Logger
	Fatal   *log.Logger
)

// Non standard init function since this one is public and takes arguments;
func Init(
	traceHandle io.Writer,
	infoHandle io.Writer,
	warningHandle io.Writer,
	errorHandle io.Writer,
	fatalHandle io.Writer) {

	Trace = log.New(traceHandle,
		"TRACE: ",
		log.Ldate|log.Ltime|log.Lshortfile)

	Info = log.New(infoHandle,
		"INFO: ",
		log.Ldate|log.Ltime|log.Lshortfile)

	Warning = log.New(warningHandle,
		"WARNING: ",
		log.Ldate|log.Ltime|log.Lshortfile)

	Error = log.New(errorHandle,
		"ERROR: ",
		log.Ldate|log.Ltime|log.Lshortfile)

	Fatal = log.New(fatalHandle,
		"ERROR: ",
		log.Ldate|log.Ltime|log.Lshortfile)

	log.SetFlags(0)
	log.SetOutput(ioutil.Discard)
}
