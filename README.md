# A Nearest Neighbor approach to the Traveling Salesman solution in Go

This project is an approximate solution to the Traveling Salesman problem using a nearest neighbor(NN) algorithm. A very detailed analysis and discussion of this relatively famous Computer Science problem is covered in reference (4).  This solution is believed to be within about 25% longer than the exact solution as noted by Wikipedia(4).  As such, this project is an exercise in implementing the NN algorithm in Go.

Some further notes about this project:
1. It is apparent from a casual inspection of the output chart that some improvments could be made to the algorithm that would reduce the distance traveled.
1. It might be possible to improve the accuracy by using a look ahead feature to consider two nodes rather than a single node in the NN algorithm.
1. Another improvement might be to implement a circle type of feature such that nodes are selected by comparison to the circle circumference.
1. Code reorganization and chart improvements are anticipated.

## Nodes
The node struct includes a Name string and x and y values as decimal.Decimal(3) types.  I used the shopspring library(3) for convenience, ease of use as well as accurate handling of the values.

A func String() applied to the node allows for convenient display of the node values.

The main function begins with the generation of 26 Named nodes, with names corresponding to the NATO phonetic alphabet; "Alfa", "Bravo", "Charlie", "Delta", "Echo", "Foxtrot", "Golf", "Hotel", "India", "Juliett", "Kilo", "Lima", "Mike", "November", "Oscar", "Papa", "Quebec", "Romeo", "Sierra", "Tango", "Uniform", "Victor", "Whiskey", "Xray"(1), "Yankee", "Zulu".  These nodes are assigned x and y coordinates randomly generated between 0 and 100.

The collection of 26 nodes are stored in a map[string]Node, Nodes.  The map key is the nodes name.

Typically this generation requires about 300 microseconds on my lenovo W540 Thinkpad as reported on this latest run:
    Generated 26 named nodes with random x y data points between 0 and 100 in 272.967µs
 
## Origin Node
An origin node, OriginNode, is identified as the nearest node to the origin point of the x and y coordinates.  This is performed by a func originNode() that computes the distance from the origin for each node and saves the lower values until all nodes have been examined. 

 ## Edges
 The edge struct is fairly straight forward and includes, node1, node2 and a length.
 
 A func String() applied to the edge allows for convenient display of the edge values.
 
The set of potential edges to be applied for each node pair are generated from the nodes x and y coordinates.  From high school algebra, perhaps you recall that the distance between 2 points on an x - y plane is given by the formula, d = sqrt((x1-x2)^2 plus (y1-y2)^2). This is always a positive number so the code applies an Absolute value function to handle this condition. This creates a set of 650 edges. Some pairs that represent the reciprocal of an edge and self referencing nodes like "Alfa" - "Alfa", are discarded or not computed.

The collection of Edges are stored in a map[string]Edge, Edges.  The map key is the concatenation of node1 and node2 strings concatenated as node1.Name - node2.Name. 

Typically this edge generation requires about 7 milliseconds as shown in the same test run output: Calculated the edge length for 650 edges between all nodes in 6.81242ms

## Route Determination
Once the maps of the Nodes and Edges have been populated we are in a position to determine the route based on the NN algorithm.  Starting with the OriginNode the func processEdges(node) finds the nearest neighbor path by scanning the edges that start at the origin node.  This nearest neighbor is the shortest path and with a few conditionals applied gives a nextNode return value.

The func findShortestEdgeFromNode(node Node) returns an Edge.  The conditionals insure the returned edge has not been used before and that the edge node2 has not been visited already.  Essentially this func iterates through the Edges and examines the edge to see if the edge node1 matches the node parameter and that the edge has not already been used and node2 of the edge has not been visited.

Edges that have been used are added to a Go slice of Edge named usedEdges.  In a similar fashion Nodes are added to another Go slice, visitedNodes.  Two small utility funcs edgeContains and nodeContains are used to check that the edge and node in question have not already been used.  When the func adds the edge (edge.node1.Name - edge.node2.Name) to the usedEdges slice it also adds the reciprocal edge, edge.node2.Name - edge.node1.Name to the usedEdges slice. 

So the func findShortestEdgeFromNode(node Node) returns the shortest edge from the current node that points to the next node in the route.  Another Go slice, routeEdge stored the edges in order that made up the shortest edges found for the nodes.

## Algorithm development and TDD
I would point out that the mechanics and operation or process of this algorith might seem straight forward but this was not the case during the construction and testing.  I was fortunate in that the test runs gave immediate feedback despite using 26 nodes as a starting point.  the performance was good enough to make tests of each func superfluous since it was easy enough to run, check the output report and make corrections. Not exactly test driven development but a reasonable compromise.
 
## Route examination
Once the func processNodes ended iterations on the nodes the visitedNodes slice contained the 26 nodes in order so it was eaasy enough to generate the route as part of the report output.

## References
1. Wikipedia contributors, "NATO phonetic alphabet," Wikipedia, The Free Encyclopedia, https://en.wikipedia.org/w/index.php?title=NATO_phonetic_alphabet&oldid=976561111 (accessed October 14, 2020). The formal NATO designation is "X-ray"
1. Traveling Salesman Algorithms, link https://cse442-17f.github.io/Traveling-Salesman-Algorithms/
1. Arbitrary-precision fixed-point decimal numbers in go. link https://github.com/shopspring/decimal
1. Wikipedia contributors, "Travelling salesman problem," Wikipedia, The Free Encyclopedia, https://en.wikipedia.org/w/index.php?title=Travelling_salesman_problem&oldid=983348561 (accessed October 14, 2020). 