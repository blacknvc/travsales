package main

import (
	"fmt"
	"strconv"
	"strings"
	"testing"
)

func TestEdge_String(t *testing.T) {
	var sbldr1 = strings.Builder{}
	var sbldr2 = strings.Builder{}
	for i := 0; i <= 26; i++ {
		sbldr1.WriteString("x" + strconv.Itoa(i) + "1, _ := routeEdge[" + strconv.Itoa(i) + "].node1.xCoord.Float64()\n")
		sbldr1.WriteString("x" + strconv.Itoa(i) + "2, _ := routeEdge[" + strconv.Itoa(i) + "].node2.xCoord.Float64()\n")
		sbldr1.WriteString("y" + strconv.Itoa(i) + "1, _ := routeEdge[" + strconv.Itoa(i) + "].node1.yCoord.Float64()\n")
		sbldr1.WriteString("y" + strconv.Itoa(i) + "2, _ := routeEdge[" + strconv.Itoa(i) + "].node2.yCoord.Float64()\n\n")

		sbldr2.WriteString("chart.ContinuousSeries{\n")
		sbldr2.WriteString("XValues: []float64{x" + strconv.Itoa(i) + "1, x" + strconv.Itoa(i) + "2},\n")
		sbldr2.WriteString("YValues: []float64{y" + strconv.Itoa(i) + "1, y" + strconv.Itoa(i) + "2},\n")
		sbldr2.WriteString("},\n\n")
	}
	fmt.Println(sbldr1.String())
	fmt.Println(sbldr2.String())
}
