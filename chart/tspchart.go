package chart

import (
	"net/http"
	"travsales/domain"
	"travsales/log"

	"github.com/wcharczuk/go-chart"
	"github.com/wcharczuk/go-chart/drawing"
)

// Charts,
// display 26 Nodes using the name as label
// and 27 Edges
func DrawChart(res http.ResponseWriter, req *http.Request) {
	x01, _ := domain.RouteEdges[0].Node1.XCoord.Float64()
	x02, _ := domain.RouteEdges[0].Node2.XCoord.Float64()
	y01, _ := domain.RouteEdges[0].Node1.YCoord.Float64()
	y02, _ := domain.RouteEdges[0].Node2.YCoord.Float64()

	x11, _ := domain.RouteEdges[1].Node1.XCoord.Float64()
	x12, _ := domain.RouteEdges[1].Node2.XCoord.Float64()
	y11, _ := domain.RouteEdges[1].Node1.YCoord.Float64()
	y12, _ := domain.RouteEdges[1].Node2.YCoord.Float64()

	x21, _ := domain.RouteEdges[2].Node1.XCoord.Float64()
	x22, _ := domain.RouteEdges[2].Node2.XCoord.Float64()
	y21, _ := domain.RouteEdges[2].Node1.YCoord.Float64()
	y22, _ := domain.RouteEdges[2].Node2.YCoord.Float64()

	x31, _ := domain.RouteEdges[3].Node1.XCoord.Float64()
	x32, _ := domain.RouteEdges[3].Node2.XCoord.Float64()
	y31, _ := domain.RouteEdges[3].Node1.YCoord.Float64()
	y32, _ := domain.RouteEdges[3].Node2.YCoord.Float64()

	x41, _ := domain.RouteEdges[4].Node1.XCoord.Float64()
	x42, _ := domain.RouteEdges[4].Node2.XCoord.Float64()
	y41, _ := domain.RouteEdges[4].Node1.YCoord.Float64()
	y42, _ := domain.RouteEdges[4].Node2.YCoord.Float64()

	x51, _ := domain.RouteEdges[5].Node1.XCoord.Float64()
	x52, _ := domain.RouteEdges[5].Node2.XCoord.Float64()
	y51, _ := domain.RouteEdges[5].Node1.YCoord.Float64()
	y52, _ := domain.RouteEdges[5].Node2.YCoord.Float64()

	x61, _ := domain.RouteEdges[6].Node1.XCoord.Float64()
	x62, _ := domain.RouteEdges[6].Node2.XCoord.Float64()
	y61, _ := domain.RouteEdges[6].Node1.YCoord.Float64()
	y62, _ := domain.RouteEdges[6].Node2.YCoord.Float64()

	x71, _ := domain.RouteEdges[7].Node1.XCoord.Float64()
	x72, _ := domain.RouteEdges[7].Node2.XCoord.Float64()
	y71, _ := domain.RouteEdges[7].Node1.YCoord.Float64()
	y72, _ := domain.RouteEdges[7].Node2.YCoord.Float64()

	x81, _ := domain.RouteEdges[8].Node1.XCoord.Float64()
	x82, _ := domain.RouteEdges[8].Node2.XCoord.Float64()
	y81, _ := domain.RouteEdges[8].Node1.YCoord.Float64()
	y82, _ := domain.RouteEdges[8].Node2.YCoord.Float64()

	x91, _ := domain.RouteEdges[9].Node1.XCoord.Float64()
	x92, _ := domain.RouteEdges[9].Node2.XCoord.Float64()
	y91, _ := domain.RouteEdges[9].Node1.YCoord.Float64()
	y92, _ := domain.RouteEdges[9].Node2.YCoord.Float64()

	x101, _ := domain.RouteEdges[10].Node1.XCoord.Float64()
	x102, _ := domain.RouteEdges[10].Node2.XCoord.Float64()
	y101, _ := domain.RouteEdges[10].Node1.YCoord.Float64()
	y102, _ := domain.RouteEdges[10].Node2.YCoord.Float64()

	x111, _ := domain.RouteEdges[11].Node1.XCoord.Float64()
	x112, _ := domain.RouteEdges[11].Node2.XCoord.Float64()
	y111, _ := domain.RouteEdges[11].Node1.YCoord.Float64()
	y112, _ := domain.RouteEdges[11].Node2.YCoord.Float64()

	x121, _ := domain.RouteEdges[12].Node1.XCoord.Float64()
	x122, _ := domain.RouteEdges[12].Node2.XCoord.Float64()
	y121, _ := domain.RouteEdges[12].Node1.YCoord.Float64()
	y122, _ := domain.RouteEdges[12].Node2.YCoord.Float64()

	x131, _ := domain.RouteEdges[13].Node1.XCoord.Float64()
	x132, _ := domain.RouteEdges[13].Node2.XCoord.Float64()
	y131, _ := domain.RouteEdges[13].Node1.YCoord.Float64()
	y132, _ := domain.RouteEdges[13].Node2.YCoord.Float64()

	x141, _ := domain.RouteEdges[14].Node1.XCoord.Float64()
	x142, _ := domain.RouteEdges[14].Node2.XCoord.Float64()
	y141, _ := domain.RouteEdges[14].Node1.YCoord.Float64()
	y142, _ := domain.RouteEdges[14].Node2.YCoord.Float64()

	x151, _ := domain.RouteEdges[15].Node1.XCoord.Float64()
	x152, _ := domain.RouteEdges[15].Node2.XCoord.Float64()
	y151, _ := domain.RouteEdges[15].Node1.YCoord.Float64()
	y152, _ := domain.RouteEdges[15].Node2.YCoord.Float64()

	x161, _ := domain.RouteEdges[16].Node1.XCoord.Float64()
	x162, _ := domain.RouteEdges[16].Node2.XCoord.Float64()
	y161, _ := domain.RouteEdges[16].Node1.YCoord.Float64()
	y162, _ := domain.RouteEdges[16].Node2.YCoord.Float64()

	x171, _ := domain.RouteEdges[17].Node1.XCoord.Float64()
	x172, _ := domain.RouteEdges[17].Node2.XCoord.Float64()
	y171, _ := domain.RouteEdges[17].Node1.YCoord.Float64()
	y172, _ := domain.RouteEdges[17].Node2.YCoord.Float64()

	x181, _ := domain.RouteEdges[18].Node1.XCoord.Float64()
	x182, _ := domain.RouteEdges[18].Node2.XCoord.Float64()
	y181, _ := domain.RouteEdges[18].Node1.YCoord.Float64()
	y182, _ := domain.RouteEdges[18].Node2.YCoord.Float64()

	x191, _ := domain.RouteEdges[19].Node1.XCoord.Float64()
	x192, _ := domain.RouteEdges[19].Node2.XCoord.Float64()
	y191, _ := domain.RouteEdges[19].Node1.YCoord.Float64()
	y192, _ := domain.RouteEdges[19].Node2.YCoord.Float64()

	x201, _ := domain.RouteEdges[20].Node1.XCoord.Float64()
	x202, _ := domain.RouteEdges[20].Node2.XCoord.Float64()
	y201, _ := domain.RouteEdges[20].Node1.YCoord.Float64()
	y202, _ := domain.RouteEdges[20].Node2.YCoord.Float64()

	x211, _ := domain.RouteEdges[21].Node1.XCoord.Float64()
	x212, _ := domain.RouteEdges[21].Node2.XCoord.Float64()
	y211, _ := domain.RouteEdges[21].Node1.YCoord.Float64()
	y212, _ := domain.RouteEdges[21].Node2.YCoord.Float64()

	x221, _ := domain.RouteEdges[22].Node1.XCoord.Float64()
	x222, _ := domain.RouteEdges[22].Node2.XCoord.Float64()
	y221, _ := domain.RouteEdges[22].Node1.YCoord.Float64()
	y222, _ := domain.RouteEdges[22].Node2.YCoord.Float64()

	x231, _ := domain.RouteEdges[23].Node1.XCoord.Float64()
	x232, _ := domain.RouteEdges[23].Node2.XCoord.Float64()
	y231, _ := domain.RouteEdges[23].Node1.YCoord.Float64()
	y232, _ := domain.RouteEdges[23].Node2.YCoord.Float64()

	x241, _ := domain.RouteEdges[24].Node1.XCoord.Float64()
	x242, _ := domain.RouteEdges[24].Node2.XCoord.Float64()
	y241, _ := domain.RouteEdges[24].Node1.YCoord.Float64()
	y242, _ := domain.RouteEdges[24].Node2.YCoord.Float64()

	x251, _ := domain.RouteEdges[25].Node1.XCoord.Float64()
	x252, _ := domain.RouteEdges[25].Node2.XCoord.Float64()
	y251, _ := domain.RouteEdges[25].Node1.YCoord.Float64()
	y252, _ := domain.RouteEdges[25].Node2.YCoord.Float64()

	nodeXFloats := []float64{}
	nodeYFloats := []float64{}

	annotations := []chart.Value2{}
	for _, node := range domain.Nodes {
		x, _ := node.XCoord.Float64()
		y, _ := node.YCoord.Float64()
		name := chart.Value2{XValue: x, YValue: y, Label: node.Name}
		nodeXFloats = append(nodeXFloats, x)
		nodeYFloats = append(nodeYFloats, y)
		annotations = append(annotations, name)
	}
	graph := chart.Chart{
		Title: "Traveling Salesman Route",
		TitleStyle: chart.Style{
			FontColor: chart.ColorWhite,
		},

		Height: domain.GridMax * 10,
		Width:  domain.GridMax * 10,

		XAxis: chart.XAxis{
			//Name: "X Axis",
			Range: &chart.ContinuousRange{
				Min: -10.0,
				Max: 110.0,
			},
			Style: chart.Style{
				FontColor: chart.ColorWhite,
			},
		},
		YAxis: chart.YAxis{
			//Name:     "Y Axis",
			AxisType: chart.YAxisSecondary,
			Range: &chart.ContinuousRange{
				Min: -10.0,
				Max: 110.0,
			},
			Style: chart.Style{
				FontColor: chart.ColorWhite,
			},
		},
		Canvas: chart.Style{
			Padding:   chart.BoxZero,
			FillColor: drawing.ColorFromHex("efefef"),
		},
		Background: chart.Style{
			Padding: chart.Box{
				Top:    50,
				Left:   25,
				Right:  25,
				Bottom: 10,
			},
			FillColor: drawing.ColorBlue,
		},
		Series: []chart.Series{
			chart.ContinuousSeries{
				XValues: []float64{x01, x02},
				YValues: []float64{y01, y02},
			},

			chart.ContinuousSeries{
				XValues: []float64{x11, x12},
				YValues: []float64{y11, y12},
			},

			chart.ContinuousSeries{
				XValues: []float64{x21, x22},
				YValues: []float64{y21, y22},
			},

			chart.ContinuousSeries{
				XValues: []float64{x31, x32},
				YValues: []float64{y31, y32},
			},

			chart.ContinuousSeries{
				XValues: []float64{x41, x42},
				YValues: []float64{y41, y42},
			},

			chart.ContinuousSeries{
				XValues: []float64{x51, x52},
				YValues: []float64{y51, y52},
			},

			chart.ContinuousSeries{
				XValues: []float64{x61, x62},
				YValues: []float64{y61, y62},
			},

			chart.ContinuousSeries{
				XValues: []float64{x71, x72},
				YValues: []float64{y71, y72},
			},

			chart.ContinuousSeries{
				XValues: []float64{x81, x82},
				YValues: []float64{y81, y82},
			},

			chart.ContinuousSeries{
				XValues: []float64{x91, x92},
				YValues: []float64{y91, y92},
			},

			chart.ContinuousSeries{
				XValues: []float64{x101, x102},
				YValues: []float64{y101, y102},
			},

			chart.ContinuousSeries{
				XValues: []float64{x111, x112},
				YValues: []float64{y111, y112},
			},

			chart.ContinuousSeries{
				XValues: []float64{x121, x122},
				YValues: []float64{y121, y122},
			},

			chart.ContinuousSeries{
				XValues: []float64{x131, x132},
				YValues: []float64{y131, y132},
			},

			chart.ContinuousSeries{
				XValues: []float64{x141, x142},
				YValues: []float64{y141, y142},
			},

			chart.ContinuousSeries{
				XValues: []float64{x151, x152},
				YValues: []float64{y151, y152},
			},

			chart.ContinuousSeries{
				XValues: []float64{x161, x162},
				YValues: []float64{y161, y162},
			},

			chart.ContinuousSeries{
				XValues: []float64{x171, x172},
				YValues: []float64{y171, y172},
			},

			chart.ContinuousSeries{
				XValues: []float64{x181, x182},
				YValues: []float64{y181, y182},
			},

			chart.ContinuousSeries{
				XValues: []float64{x191, x192},
				YValues: []float64{y191, y192},
			},

			chart.ContinuousSeries{
				XValues: []float64{x201, x202},
				YValues: []float64{y201, y202},
			},

			chart.ContinuousSeries{
				XValues: []float64{x211, x212},
				YValues: []float64{y211, y212},
			},

			chart.ContinuousSeries{
				XValues: []float64{x221, x222},
				YValues: []float64{y221, y222},
			},

			chart.ContinuousSeries{
				XValues: []float64{x231, x232},
				YValues: []float64{y231, y232},
			},

			chart.ContinuousSeries{
				XValues: []float64{x241, x242},
				YValues: []float64{y241, y242},
			},

			chart.ContinuousSeries{
				XValues: []float64{x251, x252},
				YValues: []float64{y251, y252},
			},
			chart.AnnotationSeries{
				Annotations: annotations,
			},
		},
	}
	graph.Elements = []chart.Renderable{
		chart.Legend(&graph),
	}
	res.Header().Set("Content-Type", chart.ContentTypePNG)
	err := graph.Render(chart.PNG, res)
	if err != nil {
		log.Fatal.Println(err.Error())
	}
	//f, _ := os.Create("output.png")
	//defer f.Close()
	//graph.Render(chart.PNG, f)
}
