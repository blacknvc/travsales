package main

import (
	"io/ioutil"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
	"travsales/chart"
	"travsales/domain"
	"travsales/log"
)

var (
	sbldr      = strings.Builder{}
	start      = time.Now()
	LIST_EDGES = false
	SUCCESS    bool
)

func init() {
	log.Init(ioutil.Discard, os.Stdout, os.Stdout, os.Stderr, os.Stderr)
	rand.Seed(time.Now().UnixNano())
}
func main() {
	log.Info.Println("Starting Traveling Salesman exercise.")
	sbldr.WriteString("Starting Traveling Salesman exercise, " + start.Format("2006-01-02 15:04:05"))

	sbldr.WriteString(domain.GenNodes())
	//sbldr.WriteString(domain.GenNodesFixed())
	sbldr.WriteString(domain.FindOriginNode(false))
	sbldr.WriteString(domain.CalculateEdges(false))

	sbldr.WriteString("\n\nCalculating Route starting with OriginNode:")
	sbldr.WriteString(domain.CalculateRouteFromOriginNode())
	sbldr.WriteString("\n\nRoute:")
	if len(domain.VisitedNodes) <= len(domain.Nodes)-1 {
		sbldr.WriteString("\n\tFailed! " + "Salesman did not visit all nodes. Unvisited nodes:")
		for _, node := range domain.Nodes {
			if !domain.NodeContains(domain.VisitedNodes, node) {
				sbldr.WriteString("\n\t" + node.String())
			}
		}
	} else {
		SUCCESS = true
		sbldr.WriteString("\n\tSuccess!  The salesman visited " + strconv.Itoa(len(domain.Nodes)) +
			" nodes, including back to the origin, " + domain.OriginNode.String())
	}
	sbldr.WriteString("\n\tThe salesman's route was: ")
	for i, node := range domain.VisitedNodes {
		if i == 0 {
			sbldr.WriteString("\n\t" + node.Name)
		} else {
			sbldr.WriteString(" --> " + node.Name)
		}
	}
	sbldr.WriteString(" --> " + domain.OriginNode.Name)
	lastNode := domain.Nodes[domain.VisitedNodes[len(domain.VisitedNodes)-1].Name]
	domain.TotalDistance.Add(domain.Edges[lastNode.Name+" - "+domain.OriginNode.Name].Length)
	sbldr.WriteString("\n\tThe total distance the salesman traveled was " + domain.TotalDistance.Round(2).String())

	if LIST_EDGES {
		sbldr.WriteString("\n\nEdges used in the route between all nodes (in order):")
		for _, edge := range domain.RouteEdges {
			sbldr.WriteString("\n\t" + edge.String())
		}
	}
	sbldr.WriteString("\n\nEnd of Traveling Salesman report.\n\n")

	elapsed := time.Since(start)
	sbldr.WriteString("Full execution took " + elapsed.String())

	reportStart := time.Now()
	report(sbldr.String())
	elapsedReport := time.Since(reportStart)
	log.Info.Println("Full execution took " + elapsed.String())
	log.Info.Println("Writing the file outputs took " + elapsedReport.String())

	// Scatter chart and http server
	if SUCCESS {
		http.HandleFunc("/", chart.DrawChart)
		log.Fatal.Println(http.ListenAndServe(":8080", nil))
	}
}

// Reporting
func report(s string) {
	f, err := os.Create("/home/ivan/workspace-go/src/travsales/TravelingSalesman.txt")
	if err != nil {
		log.Error.Println(err)
		return
	}
	l, err := f.WriteString(sbldr.String())
	if err != nil {
		log.Error.Println(err)
		err := f.Close()
		if err != nil {
			log.Error.Println(err)
		}
		return
	}
	log.Info.Println(l, "bytes written successfully")
	err = f.Close()
	if err != nil {
		log.Error.Println(err)
		return
	}
}
