# Go - https://golang.org/cmd/go/
# Go parameters
GOCMD=go
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
STATICCHECK=staticcheck ./...
GOGET=$(GOCMD) get
GOINSTALL=$(GOCMD) install -a
BINARY_NAME=travsales
BINARY_UNIX=$(BINARY_NAME)_unix

all: test build
build:
		$(GOBUILD) $(BINARY_NAME)
test:
		$(GOTEST) ./...
clean:
		$(GOCLEAN)
		rm -f $(BINARY_NAME)
		rm -f $(BINARY_UNIX)
install:
		$(GOINSTALL)
run:
		$(GOBUILD) $(BINARY_NAME) ./...
		$(BINARY_NAME)
sc:
		$(STATICCHECK)
deps:
		$(GOGET) github.com/shopspring/decimal
		$(GOGET) github.com/wcharczuk/go-chart
		$(GOGET) github.com/wcharczuk/go-chart/drawing

# Cross compilation
build-linux:
		CGO_ENABLED=0 GOOS=linux GOARCH=amd64 $(GOBUILD) -o $(BINARY_UNIX) -v
docker-build:
		docker run --rm -it -v "$(GOPATH)":/go -w /go/src/bitbucket.org/rsohlich/makepost golang:latest go build -o "$(BINARY_UNIX)" -v
