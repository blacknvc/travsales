package domain

import (
	"math"
	"math/rand"
	"strconv"
	"strings"

	"github.com/shopspring/decimal"

	"travsales/log"
)

var (
	NodeNames     = [26]string{"Alfa", "Bravo", "Charlie", "Delta", "Echo", "Foxtrot", "Golf", "Hotel", "India", "Juliett", "Kilo", "Lima", "Mike", "November", "Oscar", "Papa", "Quebec", "Romeo", "Sierra", "Tango", "Uniform", "Victor", "Whiskey", "Xray", "Yankee", "Zulu"}
	GridMax       = 100
	GridMin       = 0
	OriginNode    Node
	VisitedNodes  []Node
	Nodes         = make(map[string]Node)
	Edges         = make(map[string]Edge)
	UsedEdges     []Edge
	RouteEdges    []Edge
	MaxEdge       Edge
	NextNode      Node
	TotalDistance = decimal.Zero
	EdgeCount     = 27
)

func CalculateRouteFromOriginNode() string {
	var sbldr = strings.Builder{}
	for i := 0; i <= len(Nodes); i++ {
		log.Trace.Println("processing edge " + NextNode.Name)
		ProcessEdges(NextNode)
	}
	sbldr.WriteString("\n\tProcessed " + strconv.Itoa(EdgeCount) + " edges with total length " + TotalDistance.String())
	return sbldr.String()
}

func CalculateRouteFromAlternateNode(s string) string {
	OriginNode = Nodes[s]
	NextNode = Nodes[s]
	var sbldr = strings.Builder{}
	for i := 0; i <= len(Nodes); i++ {
		//sbldr.WriteString("processing edge " + NextNode.Name)
		ProcessEdges(NextNode)
	}
	sbldr.WriteString("Starting from node " + OriginNode.Name + " Processed " + strconv.Itoa(EdgeCount) + " edges with total length " + TotalDistance.String())
	return sbldr.String()
}
func ProcessEdges(node Node) {
	if NodeContains(VisitedNodes, node) {
		return
	}
	EdgeCount += 1
	VisitedNodes = append(VisitedNodes, node)
	edge := findShortestEdgeFromNode(node)
	RouteEdges = append(RouteEdges, edge)
	TotalDistance = TotalDistance.Add(edge.Length)
	NextNode = Nodes[edge.Node2.Name]
	log.Trace.Println("processing next node" + edge.Node2.Name)
}
func Process2Edges(node Node) {
	if NodeContains(VisitedNodes, node) {
		return
	}
	EdgeCount += 1
	VisitedNodes = append(VisitedNodes, node)
	edge := findShortestEdgeFromNode(node)
	RouteEdges = append(RouteEdges, edge)
	TotalDistance = TotalDistance.Add(edge.Length)
	NextNode = Nodes[edge.Node2.Name]
	log.Info.Println("processing next node" + edge.Node2.Name)
}
func findShortestEdgeFromNode(node Node) Edge {
	minimumEdge := MaxEdge
	for _, edge := range Edges {
		if edge.Node1.Name != node.Name || EdgeContains(UsedEdges, edge) {
			continue
		}
		if edge.Length.LessThan(minimumEdge.Length) && !NodeContains(VisitedNodes, edge.Node2) {
			log.Trace.Println("\nReplaced minimumEdge with shorter edge " + edge.String())
			minimumEdge = edge
		}
	}
	if len(VisitedNodes) == 26 {
		log.Info.Println("\n\tLast node " + node.Name + ", back to origin node " + OriginNode.Name)
		return Edges[node.Name+" - "+OriginNode.Name]
	}
	log.Trace.Println("\n\tShortest edge from " + node.Name + " that has not been used " + minimumEdge.String())
	rcpEdge := Edges[minimumEdge.Node2.Name+" - "+minimumEdge.Node1.Name]
	UsedEdges = append(UsedEdges, minimumEdge)
	UsedEdges = append(UsedEdges, rcpEdge)
	return minimumEdge
}
func lookAhead1Node(edge Edge) {
	edges2 := Edge{}
	for _, edge2 := range Edges {
		l2 := edge.Length.Add(edge2.Length)
		if edges2.Length.LessThan(l2) && !NodeContains(VisitedNodes, edge2.Node2) {
			log.Trace.Println("\nReplaced edges2 with shorter length " + edge.String() + "")
			edges2 = edge2
		}
	}
}

// Edges
type Edge struct {
	Node1  Node
	Node2  Node
	Length decimal.Decimal
}

func (e Edge) String() string {
	return e.Node1.Name + " - " + e.Node2.Name + ", l=" + e.Length.String()
}
func NewEdge(n1, n2 Node, d decimal.Decimal) Edge {
	e := Edge{
		Node1:  n1,
		Node2:  n2,
		Length: d,
	}
	return e
}

func CalculateEdges(listEdges bool) string {
	var sbldr = strings.Builder{}
	for _, co := range Nodes {
		for _, ci := range Nodes {
			if co.Name == ci.Name {
				log.Trace.Println("Skipping " + co.Name + " and " + ci.Name)
				continue
			}
			diffXSq := co.XCoord.Sub(ci.XCoord).Abs().Pow(decimal.NewFromInt(2))
			diffYSq := co.YCoord.Sub(ci.YCoord).Abs().Pow(decimal.NewFromInt(2))
			sumSq, _ := diffXSq.Add(diffYSq).Float64()
			d := math.Sqrt(sumSq)
			Edges[co.Name+" - "+ci.Name] = NewEdge(co, ci, decimal.NewFromFloat(d))
		}
	}
	sbldr.WriteString("\n\nCalculated the edge length for " + strconv.Itoa(len(Edges)) + " edges between all nodes. ")
	if listEdges {
		sbldr.WriteString("\nEdges:")
		for _, edge := range Edges {
			sbldr.WriteString("\n\t" + edge.String())
		}
	}
	findMaxEdgeLength()
	return sbldr.String()
}

func EdgeContains(s []Edge, e Edge) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}
func findMaxEdgeLength() {
	xEdge := Edge{Length: decimal.Zero}
	for _, edge := range Edges {
		if edge.Length.GreaterThan(xEdge.Length) {
			xEdge = edge
		}
	}
	MaxEdge = xEdge
}

// Nodes
type Node struct {
	Name   string
	XCoord decimal.Decimal
	YCoord decimal.Decimal
}

func (n *Node) String() string {
	return n.Name + ": x=" + n.XCoord.String() + ", y=" + n.YCoord.String()
}

func GenNodes() string {
	var sbldr = strings.Builder{}
	for _, node := range NodeNames {
		x := decimal.NewFromInt(int64(rand.Intn(GridMax-GridMin) + GridMin))
		y := decimal.NewFromInt(int64(rand.Intn(GridMax-GridMin) + GridMin))
		if !colocation(x, y) {
			Nodes[node] = *NewNode(node, x, y)
		} else {
			log.Error.Println("Found colocated instance")
		}
	}
	sbldr.WriteString("\n\nGenerated " + strconv.Itoa(len(Nodes)) + " named nodes with random x y data points between " +
		strconv.Itoa(GridMin) + " and " + strconv.Itoa(GridMax))
	return sbldr.String()
}
func GenNodesFixed() string {
	var sbldr = strings.Builder{}
	xray := []float64{100, 56.84, 1.87, 30, 92.66, 81.67, 16, 9.04, 72.8, 97.2, 40.11, 0.1, 46.24, 98.88, 67.12, 5.81, 20.79, 86.21, 89.11, 24.49, 3.91, 62.91, 99.62, 50.66, 0.56, 35.82, 95.56}
	yray := []float64{50, 99.53, 63.55, 4.17, 23.92, 88.69, 86.66, 21.33, 5.5, 66.5, 99.01, 46.9, 0.14, 39.46, 96.98, 73.39, 9.42, 15.52, 81.15, 93, 30.61, 1.69, 56.18, 100, 57.49, 2.05, 29.4}
	i := 0
	for _, node := range NodeNames {
		x := decimal.NewFromFloat(xray[i])
		y := decimal.NewFromFloat(yray[i])
		Nodes[node] = *NewNode(node, x, y)
		i++
	}
	sbldr.WriteString("\n\nGenerated " + strconv.Itoa(len(Nodes)) + " named nodes with x y data points between " +
		strconv.Itoa(GridMin) + " and " + strconv.Itoa(GridMax))
	for _, node := range Nodes {
		sbldr.WriteString(", " + node.String())
	}
	radius := decimal.NewFromInt(50)
	c := radius.Mul(decimal.NewFromFloat(2.0).Mul(decimal.NewFromFloat(math.Pi).Round(2)))
	sbldr.WriteString("\n\tA circle of radius " + radius.String() + " has a circumference, c = 2 * pi * radius " + c.String())
	return sbldr.String()
}
func NewNode(name string, xcoord, ycoord decimal.Decimal) *Node {
	c := Node{
		Name:   name,
		XCoord: xcoord,
		YCoord: ycoord,
	}
	return &c
}

func NodeContains(s []Node, n Node) bool {
	for _, a := range s {
		if a == n {
			return true
		}
	}
	return false
}
func FindOriginNode(listNodes bool) string {
	originDist := float64(100 * 100)
	for _, node := range Nodes {
		sqd, _ := node.XCoord.Pow(decimal.NewFromInt(2)).Add(node.YCoord.Pow(decimal.NewFromInt(2))).Float64()
		d := math.Sqrt(sqd)
		if d < originDist {
			originDist = d
			OriginNode = node
		}
	}
	var sbldr = strings.Builder{}
	sbldr.WriteString("\nClosest node to origin is " + OriginNode.String())
	if listNodes {
		sbldr.WriteString("\nNodes: ")
		for _, node := range Nodes {
			sbldr.WriteString("\n\t" + node.String())
		}
	}
	NextNode = OriginNode
	return sbldr.String()
}

func colocation(x, y decimal.Decimal) bool {
	for _, loc := range Nodes {
		return loc.XCoord.Equal(x) && loc.YCoord.Equal(y)
	}
	return false
}
