package domain

import (
	"io/ioutil"
	"math"
	"math/rand"
	"os"
	"strings"
	"testing"
	"time"
	"travsales/log"

	"github.com/shopspring/decimal"
)

var (
	visitedNodes = []Node{}
	usedEdges    = []Edge{}
	minimumEdges = []Edge{}
)

func init() {
	log.Init(ioutil.Discard, os.Stdout, os.Stdout, os.Stderr, os.Stderr)
	rand.Seed(time.Now().UnixNano())
	GenNodes()
	CalculateEdges(false)
}
func TestMinimumSpanningTree(t *testing.T) {
	for i := 0; i <= 25; i++ {
		minEdge := minimumEdge()
		if !EdgeContains(minimumEdges, minEdge) {
			minimumEdges = append(minimumEdges, minEdge)
		}
	}
	for _, edge := range minimumEdges {
		log.Info.Println("Edge " + edge.String())
	}
}

func minimumEdge() Edge {
	minEdge := MaxEdge
	for _, node := range Nodes {
		if NodeContains(visitedNodes, node) {
			continue
		}
		for _, edge := range Edges {
			if edge.Node2 == node || edge.Node1 == node {
				if edge.Length.LessThan(minEdge.Length) {
					minEdge = edge
				}
			}
		}
	}
	log.Trace.Println("MinEdge " + minEdge.String())
	visitedNodes = append(visitedNodes, minEdge.Node1)
	visitedNodes = append(visitedNodes, minEdge.Node2)
	return minEdge
}
func TestGenNodesFixed(t *testing.T) {
	log.Info.Println(GenNodesFixed())
}

/*
Foxtrot: x=23, y=77,
Lima: x=48, y=96,
Papa: x=68, y=84,
Romeo: x=73, y=76,
Zulu: x=96, y=50,
Charlie: x=16, y=32, Golf: x=32, y=16, Hotel: x=32, y=84, Sierra: x=80, y=32, Tango: x=80, y=72, Yankee: x=96, y=56, Alfa: x=4, y=50, Bravo: x=8, y=60, Delta: x=16, y=68, India: x=40, y=8, Juliett: x=40, y=92, Mike: x=56, y=10, Oscar: x=64, y=16, Quebec: x=72, y=24, Uniform: x=84, y=40, Whiskey: x=88, y=48, Xray: x=88, y=52, Echo: x=24, y=24, Kilo: x=48, y=4, November: x=56, y=92, Victor: x=84, y=60

*/

func TestGenCircle(t *testing.T) {
	var sbldrX = strings.Builder{}
	var sbldrY = strings.Builder{}
	x := 0.0
	y := 0.0

	for i := 0; i <= 26; i++ {
		x = 50.0 + 50.0*math.Cos(14.0*float64(i))
		y = 50.0 + 50.0*math.Sin(14.0*float64(i))
		sbldrX.WriteString(decimal.NewFromFloat(x).Abs().Round(2).String() + ", ")
		sbldrY.WriteString(decimal.NewFromFloat(y).Abs().Round(2).String() + ", ")
	}
	log.Info.Println("\nx, " + sbldrX.String() + "\ny, " + sbldrY.String())
}

// Generate 26 points that form a circle centered at 50,50 such that each edge is a well known length.
