package pointsLines

import "github.com/shopspring/decimal"

// XY Points, Lines, currently unused.
type point struct {
	x decimal.Decimal
	y decimal.Decimal
}

func (p point) String() string {
	return "(" + p.x.String() + ", " + p.y.String() + ")"
}

type line struct {
	p1       point
	p2       point
	midPoint point
}

func NewLine(p1, p2 point) line {
	return line{
		p1: p1,
		p2: p2,
		midPoint: point{
			x: p2.x.Sub(p1.x).Div(decimal.NewFromInt(2)).Abs(),
			y: p2.y.Sub(p1.y).Div(decimal.NewFromInt(2)).Abs(),
		},
	}
}
func (l line) String() string {
	return "p1" + l.p1.String() + ", p2" + l.p2.String() + ", midpoint" + l.midPoint.String()
}
